package phat.android.apps;

import android.os.Bundle;
import sim.android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import phat.android.app.camera.Preview;

public class CameraCapturePatientActivity extends Activity {
    Preview mPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_camera_capture);
        FrameLayout container = new FrameLayout(this);
        addCamera(container);
        //setFullScreen();
        container.addView(mPreview);

        setContentView(container);
    }

    /**
     * Sets full screen for the activity
     */
    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.camera_capture, menu);
        return true;
    }

    private void addCamera(FrameLayout container) {
        mPreview = new Preview(this);
    }
}
