package phat.android.apps;

import phat.android.app.mic.AudioTools;
import java.util.concurrent.Executor;
import sim.android.media.AudioRecord;
//import android.media.AudioRecord;
import android.app.Activity;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import phat.android.apps.R;

public class IntensityMicActivity extends Activity {
	private static String TAG = "IntensityMicActivity";
	
	private static final int UPDATE_RMS = 0x001;
	
	private TextView audioLevel;
	
	private boolean processing = false;
	private EventHandler mEventHandler;
	private DirectExecutor mDirectExecutor;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_level);
		
		audioLevel = (TextView)findViewById(R.id.audioLevelTextView);
				
		Looper looper;
		if ((looper = Looper.myLooper()) != null) {
			mEventHandler = new EventHandler(looper);
		} else if ((looper = Looper.getMainLooper()) != null) {
			mEventHandler = new EventHandler(looper);
		} else {
			mEventHandler = null;
		}
		
		mDirectExecutor = new DirectExecutor();
		
		Button start = (Button) findViewById(R.id.startRMSButton);
		start.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				processing = true;				
				mDirectExecutor.execute(new RMSAudioRunnableTask());
			}
		});	
		
		Button stop = (Button) findViewById(R.id.stopRMSButton);
		stop.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				processing = false;
			}
		});	
	}
	
	class DirectExecutor implements Executor {
	     public void execute(Runnable r) {
	    	 new Thread(r).start();
	     }
	 }
	
	class RMSAudioRunnableTask implements Runnable {
		public void run() {
			process();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		processing = false;
	}
	
	private void process() {
		Log.d(TAG, "process()");
		int frequency = 44100;
		int channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_MONO;
		int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
		
		try {

			// Create a new AudioRecord object to record the audio.
			int bufferSize = AudioRecord.getMinBufferSize(frequency,
					channelConfiguration, audioEncoding);
			Log.d(TAG, "audioRecord...");
			AudioRecord audioRecord = new AudioRecord(
					MediaRecorder.AudioSource.MIC, frequency,
					channelConfiguration, audioEncoding, bufferSize);
			Log.d(TAG, "...audioRecord");
			byte[] buffer = new byte[bufferSize];
			Log.d(TAG, "startRecording...");
			audioRecord.startRecording();
                        
			while (processing) {
				int bufferReadResult = audioRecord.read(buffer, 0, bufferSize);
				String rms = AudioTools.doubleToString(AudioTools.volumeRMS(buffer, 0, bufferReadResult));
				Log.d(TAG, "obtainMessage = "+rms);
				Message m = mEventHandler.obtainMessage(UPDATE_RMS, 0, 0, rms);
				Log.d(TAG, "sendMessage = "+m);
				mEventHandler.sendMessage(m);	            
			}

			audioRecord.stop();

		} catch (Throwable t) {
			Log.e("AudioRecord", "Recording Failed");
			if(t != null && t.getMessage() != null)
				Log.e("AudioRecord", t.getMessage());
		}
	}
	
	
	
	private class EventHandler extends Handler {

		public EventHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case UPDATE_RMS:
				Log.d(TAG, "updateRMS = "+(String)msg.obj);
				audioLevel.setText((String)msg.obj);
				return;
			default:
				Log.e(TAG, "Unknown message type " + msg.what);
				return;
			}
		}
	}
}
